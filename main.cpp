#include <cryptopp/rsa.h>
#include <cryptopp/files.h>
#include <cryptopp/base64.h>
#include <cryptopp/osrng.h>
#include <string>

void Save(const std::string& filename, const CryptoPP::BufferedTransformation& bt)
{
    CryptoPP::Base64Encoder encoder(NULL, false);

    bt.CopyTo(encoder);
    encoder.MessageEnd();

    //Save(filename, encoder);

    CryptoPP::FileSink file(filename.c_str());

    encoder.CopyTo(file);
    file.MessageEnd(); //flush
}

void SavePublicKey(const std::string& filename, const CryptoPP::PublicKey& key)
{
    CryptoPP::ByteQueue queue;
    key.Save(queue);

    Save(filename, queue);
}

void SavePrivateKey(const std::string& filename, const CryptoPP::PrivateKey& key)
{
    CryptoPP::ByteQueue queue;
    key.Save(queue);

    Save(filename, queue);
}

int main()
{
    CryptoPP::AutoSeededRandomPool rnd;

    CryptoPP::RSA::PrivateKey rsaPrivate;
    rsaPrivate.GenerateRandomWithKeySize(rnd, 3072);

    CryptoPP::RSA::PublicKey rsaPublic(rsaPrivate);

    SavePublicKey("rsa.pub", rsaPublic);
    SavePrivateKey("rsa", rsaPrivate);

    return 0;
}
